package upv.com.practica5;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;


public class MainActivity extends ActionBarActivity {
    public final static String EXTRA_MESSAGE = "com.upv.practica5.MESSAGE";

    Button btnm,btnp,btns,btnu;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Obteniendo una instancia del boton show_pet_button
        btnm = (Button)findViewById(R.id.btnmanzana);
        btnp = (Button)findViewById(R.id.btnpina);
        btns = (Button)findViewById(R.id.btnsandia);
        btnu = (Button)findViewById(R.id.btnuva);


    }


    public void clickm(View v){

        Intent intent = new Intent(this,imagenuno.class);
        intent.putExtra(EXTRA_MESSAGE,"manzanatotal.png");
        startActivity(intent);
    }

    public void clickp(View v){

        Intent intent = new Intent(this,imagendos.class);
        intent.putExtra(EXTRA_MESSAGE,"pinatotal.png");
        startActivity(intent);
    }

    public void clicks(View v){

        Intent intent = new Intent(this,imagentres.class);
        intent.putExtra(EXTRA_MESSAGE,"sandiafinal.png");
        startActivity(intent);
    }

    public void clicku(View v){

        Intent intent = new Intent(this,imagencuatro.class);
        intent.putExtra(EXTRA_MESSAGE,"uvafinal.png");
        startActivity(intent);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

}
